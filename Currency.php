<?php

namespace Torbor\currency;

/**
 * Класс для обновления курсов из ЦБРФ
 *
 * Class Currency
 * @package Torbor\currency
 */
class Currency
{
    protected $list = [];

    /**
     * Объект реализующий StorageInterface
     *
     * @var StorageInterface
     */
    protected $storage;

    /**
     * Массив с кодами валют для сохранения их курсов (['USD', 'EUR', 'KZT'])
     *
     * @var array
     */
    protected $currencyCodes;

    /**
     * Currency constructor.
     * @param StorageInterface $storage
     * @param array $currencyCodes
     */
    public function __construct(StorageInterface $storage, array $currencyCodes)
    {
        $this->storage = $storage;
        $this->currencyCodes = $currencyCodes;
    }

    /**
     * Сохраняет загруженные данные в storage
     *
     * @return bool
     */
    public function save()
    {
        if (!$this->load()) {
            return false;
        }

        $this->storage->save($this->list);
        return true;
    }

    /**
     * Загружает данные из ЦБРФ в $this->list
     *
     * @return bool
     */
    protected function load()
    {
        $xml = new \DOMDocument();
        $url = 'http://cbr.ru/scripts/XML_daily.asp';

        try {
            if ($xml->load($url)) {
                $this->list = [];

                $root = $xml->documentElement;
                $items = $root->getElementsByTagName('Valute');

                /* @var $item \DOMElement */
                foreach ($items as $item) {
                    $code = $item->getElementsByTagName('CharCode')->item(0)->nodeValue;

                    if (in_array($code, $this->currencyCodes, true)) {
                        $curs = $item->getElementsByTagName('Value')->item(0)->nodeValue;
                        $this->list[$code] = (float)str_replace(',', '.', $curs);
                    }
                }

                return true;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}