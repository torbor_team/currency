# Torbor Currency

## Установка

### Composer

	php composer.phar require torbor/currency "dev-master"

или

	"torbor/currency": "dev-master"

Так же необходимо добавить в composer.json

    "repositories": [
        {
            "type": "git",
            "url":  "git@bitbucket.org:torbor_team/currency.git"
        }
    ],
    

## Использование

    $currency = new Currency($storage, $currencyCodes);
    $currency->save();
    
$storage инстанс класса реализующего интерфейс StorageInterface,
$currencyCodes массив с названиями курсов валют, которые необходимо загрузить. (['USD', 'EUR', 'KZT'])