<?php

namespace Torbor\currency;

/**
 * Interface StorageInterface
 * @package Torbor\currency
 */
interface StorageInterface
{
    /**
     * Метод описывающий сохранение курсов валют.
     *
     * @param array $currenciesList, массив курсов валют в формате ['currencyCode' => 'rate']
     * @return mixed
     */
    public function save(array $currenciesList);
}